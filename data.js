const jsonfile = require('jsonfile-promised');
const fs = require('fs');

module.exports = {

    retornData(nomeCurso){
        let caminhoCurso = __dirname + /data/ + nomeCurso + '.json';
        return jsonfile.readFile(caminhoCurso);
    },

    salvaDados(nomeCurso, tempoEstudado) {
        let caminhoCurso = __dirname + /data/ + nomeCurso + '.json';
        console.log(caminhoCurso);
        if(fs.existsSync(caminhoCurso)){
            this.adicionaTempoAoCurso(caminhoCurso, tempoEstudado);
        }
        else{
            this.criaArquivo(caminhoCurso, {})
                .then(() => {
                    this.adicionaTempoAoCurso(caminhoCurso, tempoEstudado);
                }).catch((err) => {
                    //error
                })
        }
    },

    adicionaTempoAoCurso(caminhoCurso, tempoEstudado){
        let dados = {
            ultimoTempoEstudo: new Date().toString(),
            tempo: tempoEstudado
        }
        jsonfile.writeFile(caminhoCurso, dados, {spaces: 2})
            .then(() => {
                console.log('Tempo salvo com sucesso.');
            }).catch((err) => {
                console.log(err);
            })
    },

    criaArquivo(nomeArquivo, conteudoArquivo) {
        return jsonfile.writeFile(nomeArquivo, conteudoArquivo)
            .then(() => {
                console.log('Arquivo criado')
            }).catch((err) => {
                console.log(err)
            })
    }
}