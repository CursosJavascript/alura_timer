const { ipcRenderer }  = require('electron');
const moment = require('moment');
let segundos;
let timer;
let tempo;

module.exports = {
    inicia(el){
        tempo = moment.duration(el.textContent);
        segundos = tempo.asSeconds();
        // limpando o timer antigo para não iniciar uma stack de setIntervals
        clearInterval(timer);
        timer = setInterval(() => {
            segundos++;
            el.textContent = this.formataSegundos(segundos);
        }, 1000);
    },

    para(curso){
        clearInterval(timer);
        let tempoEstudado = this.formataSegundos(segundos)
        ipcRenderer.send('curso-parado', curso, tempoEstudado);
    },

    formataSegundos(segundos){
        return moment().startOf('day').seconds(segundos).format('HH:mm:ss')
    },
}