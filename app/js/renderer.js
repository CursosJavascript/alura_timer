const { ipcRenderer } = require('electron');
const timer = require('./timer')
const data = require('../../data')

let linkSobre = document.querySelector('#link-sobre');
let botaoPlay = document.querySelector('.botao-play');
let tempo = document.querySelector('.tempo');
let curso = document.querySelector('.curso');

window.onload = () => {

    data.retornData(curso.textContent)
        .then((dados) => {
            tempo.textContent = dados.tempo
        })
}

linkSobre.addEventListener('click' , function(){
    ipcRenderer.send('abrir-janela-sobre');
});

let imagens = ['img/play-button.svg', 'img/stop-button.svg']
let play = false;
botaoPlay.addEventListener('click', () => {
    if (play){
        timer.para(curso.textContent)
        play = false;
    }
    else{
        timer.inicia(tempo);
        play = true;
    }
    // apenas inverte a posição das imagens no array
    imagens = imagens.reverse();
    botaoPlay.src = imagens[0];
});
