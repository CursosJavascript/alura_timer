//processo principal onde controla a aplicação electron

// descructing -> importa apenas o módulo app do objeto electron
const { app, BrowserWindow, ipcMain, Tray, Menu } = require('electron');
const data = require('./data')

let tray = null;

// arrow functions sendo usadas para passar uma função dentro do módulo app on
app.on('ready',  () => {
   console.log('olá mundo')
    let mainWindow = new BrowserWindow({
       width: 600,
       height: 400
    });

   //icone deve ter 16x16
   trayIcon = new Tray(__dirname + '/app/img/electron.png');

   let trayMenu = Menu.buildFromTemplate([
       {label: 'Item1', type: 'radio'},
       {label: 'Item2', type: 'radio'}
   ]);

    trayIcon.setContextMenu(trayMenu)

    // __dirname retorna o diretório atual.
    // protocolo file para usar arquivos internos
    mainWindow.loadURL(`file://${__dirname}/app/index.html`);

});

// fecha a aplicação de uma forma correta
app.on('window-all-closed', () => {
    app.quit();
});

// abre uma nova janela usando o ipc
// resolvendo o problema de multiplas janelas
let sobreWindow = null
ipcMain.on('abrir-janela-sobre', () => {
    if (sobreWindow === null){
        sobreWindow = new BrowserWindow({
            width: 300,
            height: 220,
            // remove a barrinha do topo
            frame: false,
            // nao funcional no linux
            // alwaysOnTop: true
        });

        // funcional no linux
        // https://github.com/electron/electron/issues/12445
        sobreWindow.setAlwaysOnTop(true)

        // nao impede que a janela seja aberta novamente
        sobreWindow.on('closed', () => {
            sobreWindow = null
        })
    }

    sobreWindow.loadURL(`file://${__dirname}/app/sobre.html`);

});

ipcMain.on('fechar-janela-sobre', () => {
    sobreWindow.close();
});

ipcMain.on('curso-parado', (event, curso, tempoEstudado) => {
    console.log(`O curso ${curso}, foi estudado por ${tempoEstudado}`);
    data.salvaDados(curso, tempoEstudado)
});
